﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovement : MonoBehaviour
{

    private float _speed = 7;

    void Start()
    {
           
    }

    // Update is called once per frame
    void Update()
    {
        float horizontalMovement = Input.GetAxis("Horizontal") * _speed * Time.deltaTime;
        float verticalMovement = Input.GetAxis("Vertical") * _speed * Time.deltaTime;

        transform.Translate(horizontalMovement, 0, verticalMovement);

        if (Input.GetKey(KeyCode.Space) && _speed <= 5)
        {
            StartCoroutine(SpeedBoost());
        }

    }

    private IEnumerator SpeedBoost()
    {
        _speed += 5;
        yield return new WaitForSeconds(3f);
        _speed -= 5;
    }
}
